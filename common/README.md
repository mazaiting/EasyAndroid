# Android Kotlin Quick Development Tool

#### 介绍

Android 快速开发工具

#### 开发工具

1. Android Studio 3.5.2
2. Kotlin 1.3.60

#### 项目进度

- 11 月 22 日: v0.0.1 akdt-common 添加 Toast 和 Log 模块, 协程请求添加
- 11 月 26 日: v0.0.2 akdt-common 添加 Intent 模块
- 01 月 07 日: v0.0.3 akdt-common 增加 SharedPreferences 模块
- 04 月 01 日: v0.0.4 akdt-common 库添加 SharedPreferences 加密工具的封装, 移除 room 协程库的依赖, 新增扩展函数唯一识别码的获取, 增加退出 APP 函数, 增加加密函数 MD5, SHA1, SHA256, 新增多个函数 safeLet 使用, 修复 log 日志 tag 为空时, 使用默认 LOG_TAG, 新增 Activity 扩展函数设置 App 字体缩放功能, 新增无权限和有权限拨打电话功能, 修复 Intent 中传入 null 时异常问题
- 04 月 02 日: v0.0.5 akdt-common 增加 RSA 加密, 日志本地化
- 04 月 03 日: v0.0.6 akdt-common 增加 AES 解密, 日志本地加密及日志解密功能

#### 使用

- Toast

```
toast("test")
longToast("test")
```

- Log

```
// 开启日志
LOG_DEBUG = true
// LOG 标识
LOG_TAG = "COMMON"
// 赋值文件名
LOG_FILE_NAME =
    SimpleDateFormat("yyyyMMdd", Locale.CHINA).format(System.currentTimeMillis()) + "_log.txt"
// 保存的日志文件路径
LOG_FILE_DIRECTORY = externalCacheDir
// 设置加密密码
LOG_ENCRYPT_PASSWORD = "56H3ENoS"
// 日志打印
debug("test")
error("test")
// 日志文件解密
FileUtil.decryptFile(it, "20200403_log.txt", LOG_ENCRYPT_PASSWORD)
```

- Intent

```
// 开始 Activity
startActivity<FragmentActivity>("key" to "value")
// 获取意图
intentFor<FragmentActivity>()
// 开启服务
startService<FragmentService>("key" to "value")
```

- SharedPreferences

```
// 获取实例
SpUtil.instance
// 存储键值
sp.put(key, value)
// get 与 SharedPreferences 用法相同
```

- 非对称加密

```
md5("test")
sha1("test")
sha256("test")

//如何生成密钥对
val generator = KeyPairGenerator.getInstance("RSA")
val keyPair = generator.genKeyPair()//生成密钥对
val publicKey = keyPair.public//公钥
val privateKey = keyPair.private//私钥

println("publicKey=" + Base64.encode(publicKey.encoded, Base64.NO_WRAP))
println("privateKey=" + Base64.encode(privateKey.encoded, Base64.NO_WRAP))

val inputShort = "测试"
//    私钥加密
val encryptByPrivateKey = RSACrypt.encryptByPrivateKey(inputShort, privateKey)
println("私钥加密=$encryptByPrivateKey")
//   公钥解密
val decryptByPublicKey = RSACrypt.decryptByPublicKey(encryptByPrivateKey, publicKey)
println("公钥解密=$decryptByPublicKey")

//    公钥加密
val encryptByPublicKey = RSACrypt.encryptByPublicKey(inputShort, publicKey)
println("公钥加密=$encryptByPublicKey")
//    私钥解密
val decryptByPrivateKey = RSACrypt.decryptByPrivateKey(encryptByPublicKey, privateKey)
println("私钥解密=$decryptByPrivateKey")

// AES 加密
val encryptedStr = AesCrypto.encrypt("程序园中猿的多彩生活")
debug("加密: $encryptedStr")
// AES 解密
val decryptedStr = AesCrypto.decrypt(encryptedStr)
debug("解密: $decryptedStr")
```

- Standard

```
// 安全使用
safeLet(first, second) { first, second -> }
safeLet(first, second, third) { first, second, third -> }
safeLet(first, second, third, fourth) { first, second, third, fourth -> }
safeLet(first, second, third, fourth, fifth) { first, second, third, fourth, fifth -> }
```

### 版本信息

#### common v0.0.6
1. 增加 AES 解密
2. 日志本地加密及日志解密功能

#### common v0.0.5
1. 增加日志文件存储
2. 增加 RSA 加密

#### common v0.0.4
1. 添加 SharedPreferences 加密工具的封装
2. 移除 room 协程库的依赖
3. 新增扩展函数唯一识别码的获取, 增加退出 APP 函数
4. 增加加密函数 MD5, SHA1, SHA256
5. 新增多个参数 safeLet 使用, 
6. 修复 log 日志 tag 为空时, 使用默认 LOG_TAG
7. 新增 Activity 扩展函数设置 App 字体缩放功能
8. 新增无权限和有权限拨打电话功能
9. 修复 Intent 中传入 null 时异常问题

#### common v0.0.3
1. 增加 SharedPreferences 的简化使用
2. 修改协程结果在主线程使用

#### common v0.0.2
1. 增加四大组件的简化使用与协程

#### common v0.0.1
1. 增加 Toast 与 Log 模块

#### Contribution

1. [简书地址](https://www.jianshu.com/u/5d2cb4bfeb15)
2. [码云地址](https://gitee.com/)
3. [邮箱](mailto:zaitingma@foxmail.com)
4. [新浪微博](http://blog.sina.com.cn/mazaiting)

