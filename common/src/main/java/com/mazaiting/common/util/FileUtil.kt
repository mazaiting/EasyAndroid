package com.mazaiting.common.util

import com.mazaiting.common.crypto.AesCrypto
import java.io.BufferedReader
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader

/***
 *
 *
 *                                                    __----~~~~~~~~~~~------___
 *                                   .  .   ~~//====......          __--~ ~~
 *                   -.            \_|//     |||\\  ~~~~~~::::... /~
 *                ___-==_       _-~o~  \/    |||  \\            _/~~-
 *        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *  .~       .~       |   \\ -_    /  /-   /   ||      \   /
 * /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 * |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *          '         ~-|      /|    |-~\~~       __--~~
 *                      |-~~-_/ |    |   ~\_   _-~            /\
 *                           /  \     \__   \/~                \__
 *                       _--~ _/ | .-~~____--~-/                  ~~==.
 *                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                 -_     ~\      ~~---l__i__i__i--~~_/
 *                                 _-~-__   ~)  \--______________--~~
 *                               //.-~~~-~_--~- |-------~~~~~~~~
 *                                      //.-~~~--\
 *                               神兽保佑
 *                              代码无BUG!
 * @author mazaiting
 * @date 2020/3/26
 * @description 文件工具类
 */
object FileUtil {
    /**
     * 文件大小 10 M
     */
    private const val FILE_SIZE_M_TEN = 10 * 1024 * 1024

    /**
     * 保存字符串
     * @param directory 文件路径
     * @param fileName 文件名
     * @param data 字符串
     */
    fun saveText(directory: File, fileName: String, data: String) {
        // 外部缓存路径
        if (directory.isDirectory) {
            try {
                // 创建文件
                val file = File(directory, fileName)
                // 判断文件是否存在
                if (!file.exists()) {
                    // 创建新文件
                    file.createNewFile()
                }
                // 创建文件输出流, 并在末尾追加
                val fos = FileOutputStream(file, true)
                // 写入内容
                fos.write(data.toByteArray())
                // 输出换行
                fos.write("\n".toByteArray())
                // 关闭文件
                fos.close()
            } catch (ignore: Exception) {
            }
        }
    }

    /**
     * 删除文件
     * @param directory 文件路径
     * @param fileName 文件名
     */
    fun delete(directory: File, fileName: String, fileSize: Int = FILE_SIZE_M_TEN) {
        // 外部缓存路径
        if (directory.isDirectory) {
            // 创建文件
            val file = File(directory, fileName)
            // 判断文件是否存在 与 文件大小
            if (file.exists() && file.length() > fileSize) {
                // 删除
                file.delete()
            }
        }
    }

    /**
     * 解密内容
     */
    fun decryptFile(
        directory: File,
        fileName: String,
        password: String = AesCrypto.DEFAULT_PASSWORD
    ) {
        // 检测是否为路径
        if (directory.isDirectory) {
            try {
                // 构建旧文件
                val oldFile = File(directory, fileName)
                // 构建新文件
                val newFile = File(directory, "new_$fileName")
                // 文件文件输入流
                val fr = FileReader(oldFile)
                // 创建字节流读取
                val br = BufferedReader(fr)
                // 密文
                var text = br.readLine()
                while (text != null) {
                    saveText(directory, newFile.name, AesCrypto.decrypt(text, password))
                    text = br.readLine()
                }
                br.close()
                fr.close()
            } catch (ignore: Exception) {
            }
        }
    }
}