package com.mazaiting.common.crypto

import android.util.Base64
import java.security.PrivateKey
import java.security.PublicKey
import javax.crypto.Cipher

/***
 *
 *
 *                                                    __----~~~~~~~~~~~------___
 *                                   .  .   ~~//====......          __--~ ~~
 *                   -.            \_|//     |||\\  ~~~~~~::::... /~
 *                ___-==_       _-~o~  \/    |||  \\            _/~~-
 *        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *  .~       .~       |   \\ -_    /  /-   /   ||      \   /
 * /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 * |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *          '         ~-|      /|    |-~\~~       __--~~
 *                      |-~~-_/ |    |   ~\_   _-~            /\
 *                           /  \     \__   \/~                \__
 *                       _--~ _/ | .-~~____--~-/                  ~~==.
 *                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                 -_     ~\      ~~---l__i__i__i--~~_/
 *                                 _-~-__   ~)  \--______________--~~
 *                               //.-~~~-~_--~- |-------~~~~~~~~
 *                                      //.-~~~--\
 *                               神兽保佑
 *                              代码无BUG!
 * @author mazaiting
 * @date 2020/4/2
 * @description
 */

/**
 * 非对称加密RSA加密和解密
 */
object RSACrypt {
    /**
     * 加密方法
     */
    private const val transformation = "RSA"

    /**
     * 私钥加密
     */
    fun encryptByPrivateKey(input: String, privateKey: PrivateKey): String {
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.ENCRYPT_MODE, privateKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(input.toByteArray())
        return Base64.encodeToString(encrypt, Base64.NO_WRAP)
//        return Base64.encodeBase64String(encrypt)
    }

    /**
     * 公钥解密
     */
    fun decryptByPublicKey(input: String, publicKey: PublicKey): String {
//        val decode = Base64.decodeBase64(input)
        val decode = Base64.decode(input, Base64.NO_WRAP)
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.DECRYPT_MODE, publicKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(decode)
        return String(encrypt)
    }

    /**
     * 公钥加密
     */
    fun encryptByPublicKey(input: String, publicKey: PublicKey): String {
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(input.toByteArray())
//        return Base64.encodeBase64String(encrypt)
        return Base64.encodeToString(encrypt, Base64.NO_WRAP)
    }

    /**
     * 私钥解密
     */
    fun decryptByPrivateKey(input: String, privateKey: PrivateKey): String {
//        val decode = Base64.decodeBase64(input)
        val decode = Base64.decode(input, Base64.NO_WRAP)
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.DECRYPT_MODE, privateKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(decode)
        return String(encrypt)
    }

}