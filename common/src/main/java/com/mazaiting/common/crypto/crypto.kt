package com.mazaiting.common.crypto

import java.security.MessageDigest
import java.util.*

/***
 *
 *
 *                                                    __----~~~~~~~~~~~------___
 *                                   .  .   ~~//====......          __--~ ~~
 *                   -.            \_|//     |||\\  ~~~~~~::::... /~
 *                ___-==_       _-~o~  \/    |||  \\            _/~~-
 *        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *  .~       .~       |   \\ -_    /  /-   /   ||      \   /
 * /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 * |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *          '         ~-|      /|    |-~\~~       __--~~
 *                      |-~~-_/ |    |   ~\_   _-~            /\
 *                           /  \     \__   \/~                \__
 *                       _--~ _/ | .-~~____--~-/                  ~~==.
 *                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                 -_     ~\      ~~---l__i__i__i--~~_/
 *                                 _-~-__   ~)  \--______________--~~
 *                               //.-~~~-~_--~- |-------~~~~~~~~
 *                                      //.-~~~--\
 *                               神兽保佑
 *                              代码无BUG!
 * @author mazaiting
 * @date 2020/4/1
 * @description 加解密库
 */

/**
 * md5加密字符串
 * @param str 待加密数据
 * @return 加密后的数据
 */
fun md5(str: String): String = md5(str.toByteArray())
/**
 * md5加密字符串
 * @param bytes 待加密数组
 * @return 加密后的数据
 */
fun md5(bytes: ByteArray): String = toHex(
    MessageDigest.getInstance("MD5").digest(bytes)
)

/**
 * SHA1 加密
 * @param str 待加密数据
 * @return 加密后的数据
 */
fun sha1(str: String): String = toHex(
    MessageDigest.getInstance("SHA-1").digest(str.toByteArray())
)
/**
 * SHA1 加密
 * @param bytes 待加密数据
 * @return 加密后的数据
 */
fun sha1(bytes: ByteArray): String = toHex(
    MessageDigest.getInstance("SHA-1").digest(bytes)
)
/**
 * SHA256 加密
 * @param str 待加密数据
 * @return 加密后的数据
 */
fun sha256(str: String): String = toHex(
    MessageDigest.getInstance("SHA-256").digest(str.toByteArray())
)
/**
 * SHA256 加密
 * @param bytes 待加密数据
 * @return 加密后的数据
 */
fun sha256(bytes: ByteArray): String = toHex(
    MessageDigest.getInstance("SHA-256").digest(bytes)
)

/**
 * 转换为 16 禁止
 */
fun toHex(byteArray: ByteArray): String =
    with(StringBuilder()) {
        byteArray.forEach {
            val hex = it.toInt() and (0xFF)
            val hexStr = Integer.toHexString(hex)
            if (hexStr.length == 1) {
                append("0").append(hexStr)
            } else {
                append(hexStr)
            }
        }
        toString().toUpperCase(Locale.CHINA)
    }