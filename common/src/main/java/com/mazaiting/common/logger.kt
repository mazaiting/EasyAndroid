@file:Suppress("unused", "NOTHING_TO_INLINE")

package com.mazaiting.common

import android.util.Log
import com.mazaiting.common.crypto.AesCrypto
import com.mazaiting.common.util.FileUtil
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


/***
 *
 *
 *                                                    __----~~~~~~~~~~~------___
 *                                   .  .   ~~//====......          __--~ ~~
 *                   -.            \_|//     |||\\  ~~~~~~::::... /~
 *                ___-==_       _-~o~  \/    |||  \\            _/~~-
 *        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *  .~       .~       |   \\ -_    /  /-   /   ||      \   /
 * /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 * |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *          '         ~-|      /|    |-~\~~       __--~~
 *                      |-~~-_/ |    |   ~\_   _-~            /\
 *                           /  \     \__   \/~                \__
 *                       _--~ _/ | .-~~____--~-/                  ~~==.
 *                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                 -_     ~\      ~~---l__i__i__i--~~_/
 *                                 _-~-__   ~)  \--______________--~~
 *                               //.-~~~-~_--~- |-------~~~~~~~~
 *                                      //.-~~~--\
 *                               神兽保佑
 *                              代码无BUG!
 * @author mazaiting
 * @date 2019-11-22
 * @description Logger 工具
 */

/**
 * 标记是否打印日志
 * true: 打印
 * false: 不打印
 */
var LOG_DEBUG: Boolean = false
/**
 * LOG 标识
 */
var LOG_TAG: String = "COMMON"
/**
 * 日志文件名
 */
var LOG_FILE_NAME =
    SimpleDateFormat("yyyyMMdd", Locale.CHINA).format(System.currentTimeMillis()) + "_log.txt"
/**
 * 日志文件路径
 */
var LOG_FILE_DIRECTORY: File? = null
/**
 * 日志加密密码
 */
var LOG_ENCRYPT_PASSWORD: String = ""

/**
 * VERBOSE 日志打印
 * @param message 打印字符串
 * @param thr 异常
 */
fun Any.verbose(message: Any?, thr: Throwable? = null) {
    log(this, message, thr,
        { tag, msg -> Log.v(tag, msg) },
        { tag, msg, _ -> Log.v(tag, msg, thr) })
}

/**
 * DEBUG 日志打印
 * @param message 打印字符串
 * @param thr 异常
 */
fun Any.debug(message: Any?, thr: Throwable? = null) {
    log(this, message, thr,
        { tag, msg -> Log.d(tag, msg) },
        { tag, msg, _ -> Log.d(tag, msg, thr) })
}

/**
 * INFO 日志打印
 * @param message 打印字符串
 * @param thr 异常
 */
fun Any.info(message: Any?, thr: Throwable? = null) {
    log(this, message, thr,
        { tag, msg -> Log.i(tag, msg) },
        { tag, msg, _ -> Log.i(tag, msg, thr) })
}

/**
 * WARN 日志打印
 * @param message 打印字符串
 * @param thr 异常
 */
fun Any.warn(message: Any?, thr: Throwable? = null) {
    log(this, message, thr,
        { tag, msg -> Log.w(tag, msg) },
        { tag, msg, _ -> Log.w(tag, msg, thr) })
}

/**
 * ERROR 日志打印
 * @param message 打印字符串
 * @param thr 异常
 */
fun Any.error(message: Any?, thr: Throwable? = null) {
    log(this, message, thr,
        { tag, msg -> Log.e(tag, msg) },
        { tag, msg, _ -> Log.e(tag, msg, thr) })
}

/**
 * ERROR 日志打印
 * @param message 打印字符串
 * @param thr 异常
 */
fun Any.wtf(message: Any?, thr: Throwable? = null) {
    if (thr != null) {
        Log.wtf(getSimpleName(), message?.toString() ?: "null", thr)
    } else {
        Log.wtf(getSimpleName(), message?.toString() ?: "null")
    }
}


/**
 * 获取类名
 * @return 类名
 */
inline fun Any.getSimpleName(): String = this.javaClass.simpleName

/**
 * 打印日志
 * @param any 当前类对象
 * @param message 消息内容
 * @param thr 异常信息
 * @param f Log.d(TAG, "");
 * @param ft Log.d(TAG, "", thr);
 */
private inline fun log(
    any: Any,
    message: Any?,
    thr: Throwable?,
    f: (String, String) -> Unit,
    ft: (String, String, Throwable) -> Unit
) {
    // 获取 Tag
    var tag = any.getSimpleName()
    // 检测是否为空
    if (tag.isEmpty()) {
        tag = LOG_TAG
    }
    // 是否打印
    if (LOG_DEBUG) {
        // 如果不为空
        if (null != thr) {
            // 使用异常打印
            ft(tag, message.toString(), thr)
        } else {
            // 使用普通打印
            f(tag, message.toString())
            // 保存
            LOG_FILE_DIRECTORY?.let {
                // 待加密信息
                val text = "$tag : $message"
                // 密文数据
                val encrypt =
                    if (LOG_ENCRYPT_PASSWORD.isEmpty()) {
                        AesCrypto.encrypt(text)
                    } else {
                        AesCrypto.encrypt(text, LOG_ENCRYPT_PASSWORD)
                    }
                FileUtil.saveText(it, LOG_FILE_NAME, encrypt)
            }
        }
    }
}