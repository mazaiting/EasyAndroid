package com.mazaiting.common

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Process
import android.provider.Settings
import android.telephony.TelephonyManager
import androidx.annotation.RequiresPermission
import androidx.core.app.ActivityCompat
import com.mazaiting.common.crypto.md5
import java.util.*
import kotlin.concurrent.schedule

/***
 *
 *
 *                                                    __----~~~~~~~~~~~------___
 *                                   .  .   ~~//====......          __--~ ~~
 *                   -.            \_|//     |||\\  ~~~~~~::::... /~
 *                ___-==_       _-~o~  \/    |||  \\            _/~~-
 *        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *  .~       .~       |   \\ -_    /  /-   /   ||      \   /
 * /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 * |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *          '         ~-|      /|    |-~\~~       __--~~
 *                      |-~~-_/ |    |   ~\_   _-~            /\
 *                           /  \     \__   \/~                \__
 *                       _--~ _/ | .-~~____--~-/                  ~~==.
 *                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                 -_     ~\      ~~---l__i__i__i--~~_/
 *                                 _-~-__   ~)  \--______________--~~
 *                               //.-~~~-~_--~- |-------~~~~~~~~
 *                                      //.-~~~--\
 *                               神兽保佑
 *                              代码无BUG!
 * @author mazaiting
 * @date 2020/4/1
 * @description 设备信息
 */

/**
 * 退出 APP
 * @param time 延时时间
 */
fun exitApp(time: Long = 500L) {
    // 定时关闭
    Timer().schedule(time) {
        // 杀掉进程
        Process.killProcess(Process.myPid())
    }
}

/**
 * 获取设备唯一 ID
 * @return 设备唯一 ID
 */
fun Context.getUniqueId(): String {
    // 不选用需要权限的获取 ID 方式
    val data = getAndroidId() + getSerialNumber() + getUniquePsuedoId() + getUuid()
    return md5(data).toUpperCase(Locale.CHINA)
}

/**
 * 获取 Android ID
 * @return androidId
 */
fun Context.getAndroidId(): String =
    Settings.System.getString(contentResolver, Settings.System.ANDROID_ID)

/**
 * 获取 UUID
 */
fun getUuid(): String = UUID.randomUUID().toString()

/**
 * 获取序列号
 * @return 序列号
 */
fun getSerialNumber(): String = Build.SERIAL

/**
 * 伪 IMEI
 * @return 伪 IMEI
 */
private fun getUniquePsuedoId(): String? =
    "35" +
            Build.BOARD.length % 10 +
            Build.BRAND.length % 10 +
            Build.CPU_ABI.length % 10 +
            Build.DEVICE.length % 10 +
            Build.DISPLAY.length % 10 +
            Build.HOST.length % 10 +
            Build.ID.length % 10 +
            Build.MANUFACTURER.length % 10 +
            Build.MODEL.length % 10 +
            Build.PRODUCT.length % 10 +
            Build.TAGS.length % 10 +
            Build.TYPE.length % 10 +
            Build.USER.length % 10 //13 digits


/**
 * 获取 IMEI 号, 需要 ${Manifest.permission.READ_PHONE_STATE} 权限
 * @param context 上下文
 * @return imei 号
 */
@RequiresPermission(Manifest.permission.READ_PHONE_STATE)
@SuppressLint("HardwareIds")
private fun getImei(context: Context): String {
    val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    return if (ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.READ_PHONE_STATE
        ) != PackageManager.PERMISSION_GRANTED
    ) {
        ""
    } else {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            // 获取 IMEI
            val phoneImei = telephonyManager.imei
            // 判断是否为 Null
            if (phoneImei.isNullOrEmpty()) {
                ""
            } else {
                phoneImei
            }
        } else {
            telephonyManager.deviceId
        }
    }
}