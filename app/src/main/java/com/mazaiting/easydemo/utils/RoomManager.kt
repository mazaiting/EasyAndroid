package com.mazaiting.easydemo.utils

import androidx.room.Room
import com.mazaiting.easy.app.BaseApplication
import com.mazaiting.easydemo.room.UserDatabase

/***
 *
 *
 *                                                    __----~~~~~~~~~~~------___
 *                                   .  .   ~~//====......          __--~ ~~
 *                   -.            \_|//     |||\\  ~~~~~~::::... /~
 *                ___-==_       _-~o~  \/    |||  \\            _/~~-
 *        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *  .~       .~       |   \\ -_    /  /-   /   ||      \   /
 * /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 * |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *          '         ~-|      /|    |-~\~~       __--~~
 *                      |-~~-_/ |    |   ~\_   _-~            /\
 *                           /  \     \__   \/~                \__
 *                       _--~ _/ | .-~~____--~-/                  ~~==.
 *                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                 -_     ~\      ~~---l__i__i__i--~~_/
 *                                 _-~-__   ~)  \--______________--~~
 *                               //.-~~~-~_--~- |-------~~~~~~~~
 *                                      //.-~~~--\
 *                               神兽保佑
 *                              代码无BUG!
 * @author mazaiting
 * @date 2020-01-14
 * @description 数据库管理工具
 */
class RoomManager {
  companion object {
    /**
     * 单例
     */
    val instance by lazy(LazyThreadSafetyMode.NONE) {
      RoomManager()
    }
  }
  
  /**
   * 燃气数据库
   */
  private val gasDatabase: UserDatabase =
      Room.databaseBuilder(
          BaseApplication.instance,
          UserDatabase::class.java, "user"
      ).build()
  
  /**
   * 获取用户库
   */
  fun getUserDao() = gasDatabase.userDao()
}