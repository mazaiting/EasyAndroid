package com.mazaiting.easydemo.base

import android.util.Base64
import com.mazaiting.common.LOG_DEBUG
import com.mazaiting.common.LOG_ENCRYPT_PASSWORD
import com.mazaiting.common.LOG_FILE_DIRECTORY
import com.mazaiting.common.crypto.*
import com.mazaiting.common.debug
import com.mazaiting.common.util.FileUtil
import com.mazaiting.easy.app.BaseApplication
import com.mazaiting.easy.config.BaseConfig
import java.security.KeyPairGenerator


/***
 *
 *
 *                                                    __----~~~~~~~~~~~------___
 *                                   .  .   ~~//====......          __--~ ~~
 *                   -.            \_|//     |||\\  ~~~~~~::::... /~
 *                ___-==_       _-~o~  \/    |||  \\            _/~~-
 *        __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *    _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *  .~       .~       |   \\ -_    /  /-   /   ||      \   /
 * /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 * |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *          '         ~-|      /|    |-~\~~       __--~~
 *                      |-~~-_/ |    |   ~\_   _-~            /\
 *                           /  \     \__   \/~                \__
 *                       _--~ _/ | .-~~____--~-/                  ~~==.
 *                      ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                 -_     ~\      ~~---l__i__i__i--~~_/
 *                                 _-~-__   ~)  \--______________--~~
 *                               //.-~~~-~_--~- |-------~~~~~~~~
 *                                      //.-~~~--\
 *                               神兽保佑
 *                              代码无BUG!
 * @author mazaiting
 * @date 2019-11-18
 * @description 全局 Application
 */
class EasyApplication : BaseApplication() {

    override fun getConfig(): BaseConfig = Config()

    override fun initOtherConfig() {
        initLogFile()
        super.initOtherConfig()
        debug("初始化其他配置")
        initEncrypt()
    }

    /**
     * 初始化日志文件
     */
    private fun initLogFile() {
        LOG_DEBUG = true
        // 设置保存为文件
        LOG_FILE_DIRECTORY = externalCacheDir
        // 设置加密密码
        LOG_ENCRYPT_PASSWORD = "56H3ENoS"
    }

    /**
     * 初始化加密
     */
    private fun initEncrypt() {
        debug(md5("test"))
        debug(sha1("test"))
        debug(sha256("test"))
        testAes()
        testRsa()
    }

    private fun testAes() {
        val encryptedStr = AesCrypto.encrypt("程序园中猿的多彩生活")
        debug("加密: $encryptedStr")
        val decryptedStr = AesCrypto.decrypt(encryptedStr)
        debug("解密: $decryptedStr")
        externalCacheDir?.let {
            FileUtil.decryptFile(it, "20200403_log.txt", LOG_ENCRYPT_PASSWORD)
        }
    }

    /**
     * 测试 RSA
     */
    private fun testRsa() {
        //如何生成密钥对
        val generator = KeyPairGenerator.getInstance("RSA")
        val keyPair = generator.genKeyPair()//生成密钥对
        val publicKey = keyPair.public//公钥
        val privateKey = keyPair.private//私钥

//    println("publicKey=" + Base64.encodeBase64(publicKey.encoded))
//    println("privateKey=" + Base64.encodeBase64(privateKey.encoded))
        println("publicKey=" + Base64.encode(publicKey.encoded, Base64.NO_WRAP))
        println("privateKey=" + Base64.encode(privateKey.encoded, Base64.NO_WRAP))

        val inputShort = "测试"
        //    私钥加密
        val encryptByPrivateKey = RSACrypt.encryptByPrivateKey(inputShort, privateKey)
        println("私钥加密=$encryptByPrivateKey")
        //   公钥解密
        val decryptByPublicKey = RSACrypt.decryptByPublicKey(encryptByPrivateKey, publicKey)
        println("公钥解密=$decryptByPublicKey")

        //    公钥加密
        val encryptByPublicKey = RSACrypt.encryptByPublicKey(inputShort, publicKey)
        println("公钥加密=$encryptByPublicKey")
        //    私钥解密
        val decryptByPrivateKey = RSACrypt.decryptByPrivateKey(encryptByPublicKey, privateKey)
        println("私钥解密=$decryptByPrivateKey")
    }
}









