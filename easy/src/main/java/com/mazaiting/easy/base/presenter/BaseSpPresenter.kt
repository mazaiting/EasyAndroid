package com.mazaiting.easy.base.presenter

import com.mazaiting.common.util.SpUtil
import com.mazaiting.easy.base.mvp.IBaseView


/**
 * 带SharedPreferences的Presenter
 * @author mazaiting
 */
open class BaseSpPresenter<T : IBaseView> : BasePresenter<T>() {
    /**
     * 注入 SharedPreferences 工具类
     */
    private val sp: SpUtil = SpUtil.instance

    /**
     * 保存
     * @param key 键
     * @param value 值
     */
    protected fun put(key: String, value: Boolean) = sp.put(key, value)

    /**
     * 保存
     * @param key 键
     * @param value 值
     */
    protected fun put(key: String, value: Set<String>) = sp.put(key, value)

    /**
     * 获取对应键的值
     * @param key 键
     * @return Boolean数据
     */
    protected fun getBoolean(key: String) = sp.getBoolean(key)

    /**
     * 获取对应键的值
     * @param key 键
     * @return 字符串数据
     */
    protected fun getString(key: String) = sp.getString(key)

    /**
     * 获取对应键的值
     * @param key 键
     * @return int数据
     */
    protected fun getInt(key: String) = sp.getInt(key)

    /**
     * 获取对应键的值
     * @param key 键
     * @return int数据
     */
    protected fun getLong(key: String) = sp.getLong(key)

    /**
     * 获取对应键的值
     * @param key 键
     * @return int数据
     */
    protected fun getFloat(key: String) = sp.getFloat(key)

    /**
     * 移除键
     * @param key 键
     */
    protected fun remove(key: String) = sp.remove(key)

    /**
     * 判断是否包含键
     * @param key 键
     * @return true: 包含; false: 不包含
     */
    protected fun contains(key: String): Boolean = sp.contains(key)

    /**
     * 获取所有的键值
     * @return Map
     */
    protected fun getAll(): MutableMap<String, *>? = sp.getAll()
}
